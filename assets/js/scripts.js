$(function () {
    $('header nav').meanmenu({
        meanMenuContainer: 'header .place-nav',
        meanMenuOpen: "<i class='icon-menu'></i>",
        meanMenuClose: "<i class='icon-plus'></i>",
        meanScreenWidth: 1025,
        meanDisplay: "block"
    });

    $("nav a[href^='https']").attr("target","_blank");
    $("a[name]").parent().addClass('has_target');
    $("#page h1, h2#append").appendTo("#page-title");
    $("#page .page-divider").parent("#page").addClass('has_divider');
    $(".internal .more-to-explore").appendTo(".internal");

    ///////////// global variables
    var theWindow = $(window),
        body = $("body"),
        header = $("header"),
        headerBottom = $("header").outerHeight(),
        stickyBottom = $("header #hd-top").outerHeight();

    /////////////// resize options
    $(window).on('resize', function () {
        $("body").css('padding-top', $("header").outerHeight());
        $(".mean-container .mean-nav").css('bottom', $("#fixed-tabs").outerHeight());
        if ($(window).width() <= 1025) {
            $("footer").css('padding-bottom', $("#fixed-tabs").outerHeight());
        }
    }).trigger('resize');

    /////////////// fixed header with animated in on desktop and attach on mobile
    theWindow.on("scroll", function () {
        if (theWindow.width() > 1025) {
            if (theWindow.scrollTop() >= headerBottom) {
                body.addClass("fix-nav");
                header.addClass("animated slideInDown");
            } else if (theWindow.scrollTop() <= headerBottom) {
                body.removeClass("fix-nav");
                header.removeClass("animated slideInDown");
            }
        }

        // if (theWindow.width() < 1001) {
        //     if (theWindow.scrollTop() >= headerBottom) {
        //         body.addClass('attach');
        //     } else if (theWindow.scrollTop() <= headerBottom) {
        //         body.removeClass('attach');
        //     }
        // }
    });

    if (theWindow.width() < 1026) {
        // Hide Header on on scroll down
                var didScroll;
                var lastScrollTop = 0;
                var delta = 5;
                var navbarHeight = $('header').outerHeight();

                $(window).scroll(function(event){
                    didScroll = true;
                });

                setInterval(function() {
                    if (didScroll) {
                        hasScrolled();
                        didScroll = false;
                    }
                }, 250);

                function hasScrolled() {
                    var st = $(this).scrollTop();
                    // Make sure they scroll more than delta
                    if(Math.abs(lastScrollTop - st) <= delta)
                        return;
                    // If they scrolled down and are past the navbar, add class .nav-up.
                    // This is necessary so you never see what is "behind" the navbar.
                    if (st > lastScrollTop && st > navbarHeight){
                        // Scroll Down
                        $('header').removeClass('nav-down').addClass('nav-up');
                    } else {
                        // Scroll Up
                        if(st + $(window).height() < $(document).height()) {
                            $('header').removeClass('nav-up').addClass('nav-down');
                        }
                    }
                    lastScrollTop = st;
                }
        }

    if ($(window).width() <= 1024) {
        $("footer .social").clone().prependTo(".mean-container .mean-nav");
        $("footer .hours").clone().appendTo(".mean-container .mean-nav");
    }

    $('nav ul li').each(function() {
        if ($(this).find('ul').length) {
            ($(this).addClass('has-submenu'))
        }
        });
        if (theWindow.width() > 1024) {
           $('nav > ul > .has-submenu > a').append("<span><i class='icon-down-dir'></i></span>")
           $('nav ul ul > .has-submenu > a').append("<span><i class='icon-right-dir'></i></span>")
        }




});   // end of top-most function





/////////////// convert items to slider in mobile
$(function () {
    var theWindow = $(window);



////////////////////////////// Randomized Banner Background
     var selectBG = Math.floor(Math.random() * 10) + 1;
     if (!$("body").hasClass("page_index") && !$("#page-banner").hasClass("has-img")) {
         $('#page-banner').append('<div class="banner-img">');
         $('#page-banner .banner-img').css({
             'background-image': 'url(assets/images/banner-' + selectBG + '.jpg)',
             'background-size': 'cover',
             'background-repeat': 'no-repeat',
             'background-position': 'center center'
         })
     }



    ////////////////////////////// FAQ / Accordion
    $(".accordion h3, .accordion h2").addClass("toggle");
    $(".toggle").each(function () {
        $(this).nextUntil('.toggle').add().wrapAll('<div>');
    });
    $(".toggle").on("click", function () {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active").next().slideUp();
        } else {
            $(".toggle").removeClass("active").next().slideUp();
            $(this).addClass("active").next().slideDown();
            for (var i = 0; i < $('.accordion iframe').length; i++) {
                $('.accordion iframe')[i].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
            }
        }
    });


 $( ".why ul li" ).wrapInner( "<span></span>");

    ////////////////////////////// page divider
    var dividerStart = "> h2, .page-divider .wrap",
        mobileWidth = 1000;
    $.when(setupServices()).done(function () {});
       function setupServices() {

        $(".page-divider " + dividerStart)
            .addClass("dividerLead").each(function () { //auto wrap
                $(this)
                    .nextUntil('.dividerLead')
                    .addBack()
                    .wrapAll('<div class="block">');
        });

         $(".block").each(function (index) {
                   $(this)
                       .find('.dividerLead')
                       .next(".elem")
                       .addClass('wow fadeIn')
                       .addClass(index % 2 ? 'elem-right' : 'elem-left')
                    // move .elem above H2
                   if ($(window).width() >= mobileWidth) {
                       $(this).children(".elem").insertBefore($(this).children(".dividerLead"));
                   }
               })


///////////// move anchors to top of blocks
           $(".page-divider a[name]").each(function () {
            var getAnchor = $(this).parent(".has_target"),
                anchorTarget = $(this).parentsUntil(".page-divider").next().find(".dividerLead").parent();
            getAnchor.remove();
            $(this).prependTo(anchorTarget);
        });

    }

    $(".block [class^='btn']").parent("p").addClass('has_btn');



///////// make mini-blocks for h3 inside divider blocks
    $(".page-divider h3").each(function() {
        $(this).nextUntil('h3, .dividerLead').addBack().wrapAll('<div>')
        .parent().addClass("mini-block")
    });
    $(".mini-block").each(function (index) {
        $(this)
            .find('h3')
            .next(".elem-sm")
            .addClass(index % 2 ? 'elem-left' : 'elem-right')
        if ($(window).width() >= mobileWidth) {
            $(this).children(".elem-sm").insertBefore($(this).children("h3"));
        }
    })

    ///////////// wraps text & .btn in article after .block .elem
    $(".block .elem + *, .block .wrap, .mini-block h3").each(function () { //auto wrap
        $(this).nextUntil('.block, .mini-block').addBack().wrapAll('<article>');
    });

    /// for flexing inside .blocks
    $(".block .elem").each(function () { //auto wrap
        $(this).nextUntil('.block, .mini-block').addBack().wrapAll('<div>').parent().addClass('contain');
    });


    //wrap dr credentials
    if ($("#events-grid").length) {
      $("#events-grid ul").each(function() {
        $(this).nextUntil("ul").addBack().wrapAll('<div class="event">');
      });
    }


        ////////  If no image insided block to change styles
        $('.block').each(function() {
            if (!$(this).find('.elem').length) {
                ($(this).addClass('no_img'))
            }
        });
        $(".block.no_img .dividerLead").each(function () { //auto wrap
            $(this).nextUntil('.block').addBack().wrapAll('<article>');
        });



    // page title + intro - interior banner
    ////////////  Add flex to .top img/video + text before page-dividers
    $("#page > p:first-of-type").each(function() {
        $(this).nextUntil("div, h1, h2, h3, form").addBack().wrapAll("<article id='intro'>")
    });

////////////  Add flex to .top img/video + text before page-dividers


    $('#page-title').each(function() {
        $(this).appendTo("#page-banner");
    });

    $('#page-title').each(function() {
        if ($(this).find('.play').length) {
            $(".page-banner").attr("data-player", "youtube")
        }
    });

    if($("#main-img").length) {
      $("#page-banner").addClass("has-img");
      $("#main-img").appendTo("#page-banner");
      $("#page-title").wrap('<div class="title-wrapper" />');
      $("#intro").appendTo("#page-banner .title-wrapper");
    }



});


//////// Banner and testimonial video with different top offsets
 $(function () {

    $("#banner[data-player]").tntvideos({
        playButton: '.play',
        closeButton: '.close',
        bodyPlaying: null,
        animate: true,
        mobileWidth: 900,
        mobileAppendPlay: '#banner article',
        offset:0
    });

    $("#testimonials [data-player]").tntvideos({
        playButton: '.play',
        closeButton: '.close',
        animate: true,
        mobileWidth: 900
    });


     //Basic youtube embed with close button/for internal pages
     $(".internal [data-player]").tntvideos({
         closeButton: '.close',
         playButton: '.yt-play',
         animate: false,
         mobileWidth: 900,
         offset:0,
         closeButtonString: '<span><i class="icon-plus"></i></span>'
     });



jQuery.fn.setupYoutube = function() {
    iframe = document.createElement("iframe"), iframe.setAttribute("frameborder", "0"), iframe.setAttribute("allowfullscreen", ""), iframe.setAttribute("width", this.attr("data-width")), iframe.setAttribute("height", this.attr("data-height")), iframe.setAttribute("src", "https://www.youtube.com/embed/" + this.attr("data-embed") + "?rel=0&autoplay=1&playsinline=1&enablejsapi=1"), this.prepend(iframe);
    var e = iframe.height / iframe.width * 100;
    iframe.style.position = "absolute", iframe.style.top = "0", iframe.style.left = "0", iframe.style.right = "0", iframe.width = "100%", iframe.height = "100%";
    var t = document.createElement("div");
    t.className = "fluid-vid", t.style.width = "100%", t.style.position = "relative", t.style.paddingTop = e + "%", iframe.parentNode.insertBefore(t, iframe), t.appendChild(iframe)
};

    $('#reviews .cycle-slideshow').on('cycle-before', function (event, opts) {
        setTimeout(function () {
            var currentSlide = $(opts.slides[opts.currSlide]);
            currentSlide.find('.youtube iframe').attr('src', '');
            currentSlide.find('.youtube iframe').attr('src', "https://www.youtube.com/embed/" + currentSlide.find('.youtube').attr("data-embed") + "?rel=0&showinfo=0");
        });
    });

   $('.video-grid > div iframe').wrap('<div class="videoWrapper"></div>')
 });



    $(".slick-gallery").slick({
        dots:true,
        arrows:true,
        fade: true,
        autoplay:true,
        autoplaySpeed:5000,
        speed:500,
        adaptiveHeight: true,
        appendDots:".slick-controls",
        prevArrow:'<a class="arrow" id="prev"><span class="arrow-icon"></span></a>',
        nextArrow:'<a class="arrow" id="next"><span class="arrow-icon"></span></a>',
        customPaging:function(slider,index) {
            var totalCount = "", totalSlides = ""; //to add 0
            if (slider.slideCount < 10) { totalCount = ""; }
            if (index < 9) { totalSlides ="";  }
            return '<div>' + totalSlides + (index + 1) + '</div>';
        },
        responsive:
        [{
            breakpoint: 700,
            settings: {
                appendArrows:".slick-controls",
                dots:true
            }
        }]
    });



    //Slider Form
        $(".slick-form").slick({
            dots:true,
            infinite:false,
            draggable: false,
            prevArrow:'',
            nextArrow:'.input .next',
            arrows:true,
        });
        //prevent validator on slider form
        $('footer .forms input').on('invalid', function(e) { e.preventDefault(); });
